package main

import (
  "bytes"
  "fmt"
  "io/ioutil"
  "os"
  "os/exec"
  "path"
  "path/filepath"
  "regexp"
  "strings"
)

var (
  dir = "/mnt/ceph/Movies/"
  tmpdir = "/mnt/daemons/downloads/tmp/"
  basename,ffmpegcmds,files,name,nameattr []string
  ffmpeginfo string
  renamecmds int
  err   error
)

func Contains(a []string, x string) bool {
    for _, n := range a {
        if x == n {
            return true
        }
    }
    return false
}

func ContainsV(a string) bool {
  if strings.Contains(a, ".avi") ||
     strings.Contains(a, ".m4v") ||
     strings.Contains(a, ".mp4") ||
     strings.Contains(a, ".mkv") {
    return true
  } else {
    return false
  }
}

func ContainsVmkv(a string) bool {
  if strings.Contains(a, ".mkv") {
    return true
  } else {
    return false
  }
}

func FilePathWalkDir(root string) ([]string, error) {
  var files []string
  err := filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
      if !info.IsDir() {
          files = append(files, path)
      }
      return nil
  })
  return files, err
}

func GetFfmpegInfo (basename []string) (ffmpeginfo string) {
  cmd := exec.Command("ffmpeg", "-i", basename[0] + "/" + basename[1] + basename[2])
  var outb, errb bytes.Buffer
  cmd.Stdout = &outb
  cmd.Stderr = &errb
  err := cmd.Run()
  if err != nil {
    for _, line := range strings.Split(errb.String(), "\n") {
      if strings.Contains(line, "No such file or directory") {
        fmt.Print(basename[0] + "/" + basename[1] + basename[2] + ": No such file or directory")
        os.Exit(2)
      }
      ffmpeginfo = ffmpeginfo + string(line) + "\n"
    }
  }
  return ffmpeginfo
}

func AttrACodecChannels(ffmpeginfo string) (attr string) {
  var audios, audio []string
  for _, line := range strings.Split(ffmpeginfo, "\n") {
    if strings.Contains(line, "Stream #0") {
      if strings.Contains(line, "Audio") {
        audios = append(audios, line)
      }
    }
  }
  if len(audios) == 0 {
    fmt.Print("Remove (no audio) ... ")
  }
  if len(audios) > 0 {
    for _, value := range audios {
      if strings.Contains(value, "(eng)") {
        audio = append(audio, value)
      }
    }
    if len(audio) == 0 {
      fmt.Print("No eng audio stream found ")
      fmt.Print("(Possible anime/foreign movie?) ")
      fmt.Print("Exiting...\n")
    }
    if len(audio) > 1 {
      attr = "MULTI"
    }
    if len(audio) == 1 {
      audio := strings.Split(audios[0], "Audio:")
      re := regexp.MustCompile(`\((.*?)\)`)
      audio[1] = re.ReplaceAllLiteralString(audio[1], "")
      re = regexp.MustCompile(`\[(.*?)\]`)
      audio[1] = re.ReplaceAllLiteralString(audio[1], "")
      audio[1] = strings.Replace(audio[1], " ,", ",", -1)
      audio[1] = strings.Replace(audio[1], ", ", ",", -1)
      attr = strings.TrimSpace(
                  strings.ToUpper(
                    strings.Replace(strings.Split(audio[1], ",")[0], " ", "", -1) +
                                    " " +
                                    strings.Split(audio[1], ",")[2]))
    }
  }
  return attr
}

func AttrVContainer(ffmpeginfo string) (attr string) {
  var videos, video []string
  for _, line := range strings.Split(ffmpeginfo, "\n") {
    if strings.Contains(line, "Stream #0") {
      if strings.Contains(line, "Video") {
        videos = append(videos, line)
      }
    }
  }
  if len(videos) > 1 {
    fmt.Print("Multiple video streams found! ")
    fmt.Print("Very likely file contains multiple mjpeg streams. ")
    fmt.Print("Exiting...\n")
    os.Exit(2)
  } else {
    if strings.Contains(videos[0], "(eng)") {
      video = strings.Split(string(videos[0]), "Video:")
      container := strings.TrimSpace(strings.Split(video[1], ",")[0])
      if strings.Split(container, " ")[0] == "hevc" {
        attr = "X265"
      }
      if strings.Split(container, " ")[0] == "h264" {
        attr = "X264"
      }
    } else {
      fmt.Print("No eng video streams (Anime??).  Possible fix ... \n")
      fmt.Print("mkvpropedit \"${i}\" --edit track:v1 --set language=eng\n")
    }
  }
  return attr
}

//func AttrVQuality(basename []string) (string) {
//  return attr
//}

func TransVData(ffmpeginfo string) (ffmpegcmds string) {
  var Attachment []string
  for _, line := range strings.Split(ffmpeginfo, "\n") {
    if strings.Contains(line, "Stream #0") {
      if strings.Contains(line, "Attachment") {
        Attachment = append(Attachment, line)
      }
    }
  }
  if len(Attachment) > 0 {
    ffmpegcmds="-map -0:d"
  }
  return ffmpegcmds
}

func TransVSubtitles(ffmpeginfo string) (ffmpegcmds string) {
  var streams []string
  for _, line := range strings.Split(ffmpeginfo, "\n") {
    if strings.Contains(line, "Stream #0") {
      if strings.Contains(line, "Subtitle") {
        streams = append(streams, line)
      }
    }
  }
  if len(streams) > 0 {
    ffmpegcmds="-map -0:s"
  }
  return ffmpegcmds
}

func main() {

  dirs, err := ioutil.ReadDir(dir)

  if err == nil {

    for _, d := range dirs {

      files, err = FilePathWalkDir(dir + d.Name())

      if err == nil {

        for _, file := range files {

          ffmpegcmds = make([]string, 0)
          ffmpeginfo = ""
          renamecmds = 0

          basename = []string{path.Dir(file), strings.TrimSuffix(filepath.Base(file), filepath.Ext(file)), filepath.Ext(file)}

          if ContainsV(basename[2]) {

            fmt.Print("\n" + file + "\n")

            ffmpeginfo = GetFfmpegInfo(basename)

            // Ensure filenames contains date - (1984)
            matched, err := regexp.MatchString(` \((.*?)\)`, basename[1])
            if ! matched {
               fmt.Print(err)
               fmt.Print("Date missing or malformed filename!  Exiting...\n")
               os.Exit(2)
            }

            // Build trancoding arguements
            if ContainsVmkv(basename[2]) {
              // Remove additional data and subtitles

              ffmpegcmd := TransVSubtitles(ffmpeginfo)
              if ffmpegcmd != "" {
                if len(ffmpegcmds) == 0 {
                  ffmpegcmds = append(ffmpegcmds, "-c copy")
                  ffmpegcmds = append(ffmpegcmds, "-map 0")
                }
                ffmpegcmds = append(ffmpegcmds, ffmpegcmd)
              }

              ffmpegcmd = TransVData(ffmpeginfo)
              if ffmpegcmd != "" {
                if len(ffmpegcmds) == 0 {
                  ffmpegcmds = append(ffmpegcmds, "-c copy")
                  ffmpegcmds = append(ffmpegcmds, "-map 0")
                }
                ffmpegcmds = append(ffmpegcmds, ffmpegcmd)
              }
            } else {
              // Transcode non-matroska to matroska/h265
              ffmpegcmds = append(ffmpegcmds, "-c copy")
              ffmpegcmds = append(ffmpegcmds, "-map 0")
              ffmpegcmds = append(ffmpegcmds, "-vcodec libx265")
            }

            // Ensure filename contains correct attributes [container][quality][audio codec channels]
            name = strings.SplitAfter(basename[1], ")")
            nameattr = strings.SplitAfter(name[1], "]")

            // [container]
            if nameattr[0] == "[" + "]" {
              nameattr[0] = "[" + AttrVContainer(ffmpeginfo) + "]"
              renamecmds++
            } else {
              if nameattr[0] != "[X265]" {
                ffmpegcmds = append(ffmpegcmds, "-vcodec libx265")
              }
            }

            // [source-quality] - How do I determine this??
            //  if nameattr[1] == "[" {
            //    attr[1] = AttrVQuality(ffmpeginfo)
            //    renamecmds++
            //  }

            // [audio codec channels]
            if nameattr[2] == "[" + "]" {
              nameattr[2] = "[" + AttrACodecChannels(ffmpeginfo) + "]"
              renamecmds++
            }

            // transcode and rename
            // outputs commands for copy/pasting at this time
            if len(ffmpegcmds) > 0 {

              ffmpegcmds = append(ffmpegcmds, "-acodec copy")

              fmt.Print("ffmpeg -i \"" + file + "\" ")
              for _, i := range ffmpegcmds {
                fmt.Print(i + " ")
              }

              if Contains(ffmpegcmds, "-vcodec libx265") {
                fmt.Print("\"" + tmpdir + strings.Replace(basename[1], nameattr[0], "[X265]", -1) + ".mkv" + "\"")
              } else {
                fmt.Print("\"" + tmpdir + basename[1] + ".mkv" + "\"")
              }

              fmt.Print("\n")
              fmt.Print("mv \"" + file + "\" \"" + basename[0] + "/" + basename[1] + ".old" + "\"\n")

              if Contains(ffmpegcmds, "-vcodec libx265") {
                fmt.Print("mv \"" +
                          tmpdir +
                          strings.Replace(basename[1], nameattr[0], "[X265]", -1) +
                          ".mkv" +
                          "\" \"" +
                          basename[0] +
                          "/" +
                          strings.Replace(basename[1], nameattr[0], "[X265]", -1) +
                          ".mkv" +
                          "\"\n")
              } else {
                fmt.Print("mv \"" +
                          tmpdir +
                          basename[1] +
                          ".mkv" +
                          "\" \"" +
                          basename[0] +
                          "/" +
                          basename[1] +
                          ".mkv" +
                          "\"\n")
              }

              ffmpegcmds = make([]string, 0)
              renamecmds = 0
            }

            // rename only
            if renamecmds != 0 {
              fmt.Print("mv \"" + file + "\" \"" + basename[0] + "/" + name[0] + nameattr[0] + nameattr[1] + nameattr[2] + basename[2] + "\"\n")
              os.Rename(file, basename[0] + "/" + name[0] + nameattr[0] + nameattr[1] + nameattr[2] + basename[2])
            }

          }

        }

      }

    }

  }

}
[root@cube ccallega]#