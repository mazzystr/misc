#!/usr/bin/env python

import base64
import json
import os
import shlex
import subprocess
import time
import warnings
import webbrowser

warnings.filterwarnings("ignore")

file_example_aws_environment_registry = os.environ['HOME'] + "/repos/example/conf/aws-environment-registry.json"
nagios_proto = 'http://'
nagios_urluri = '-mgt1.example.com:8811/nagios3/'
urls = [
  'https://docs.google.com/spreadsheets/d/1x9ldd3E4KQP7E6w6XJsxA7R1ll1OH-0xltcokmnFYak',
  'https://examplestatus.com/',
  'http://dashboard.example.com/',
  'http://status.example.com/',
  'https://example.pagerduty.com/incidents',
  'https://my.pingdom.com/newchecks/checks',
  'http://jarvis.example.com/nagios3'
  ]
b64_preferences = '\
ewogICAiYXBwcyI6IHsKICAgICAgInNob3J0Y3V0c192ZXJzaW9uIjogMgogICB9\
LAogICAiYXV0b2ZpbGwiOiB7CiAgICAgICJ1c2VfbWFjX2FkZHJlc3NfYm9vayI6\
IGZhbHNlCiAgIH0sCiAgICJicm93c2VyIjogewogICAgICAiY2hlY2tfZGVmYXVs\
dF9icm93c2VyIjogZmFsc2UsCiAgICAgICJsYXN0X2tub3duX2dvb2dsZV91cmwi\
OiAiaHR0cHM6Ly93d3cuZ29vZ2xlLmNvbS8iLAogICAgICAibGFzdF9wcm9tcHRl\
ZF9nb29nbGVfdXJsIjogImh0dHBzOi8vd3d3Lmdvb2dsZS5jb20vIiwKICAgICAg\
InNob3dfaG9tZV9idXR0b24iOiB0cnVlLAogICAgICAic2hvd191cGRhdGVfcHJv\
bW90aW9uX2luZm9fYmFyIjogZmFsc2UsCiAgICAgICJ3aW5kb3dfcGxhY2VtZW50\
IjogewogICAgICAgICAiYWx3YXlzX29uX3RvcCI6IHRydWUsCiAgICAgICAgICJi\
b3R0b20iOiA2ODMsCiAgICAgICAgICJsZWZ0IjogMTAsCiAgICAgICAgICJtYXhp\
bWl6ZWQiOiBmYWxzZSwKICAgICAgICAgInJpZ2h0IjogMTA2MCwKICAgICAgICAg\
InRvcCI6IDIyLAogICAgICAgICAid29ya19hcmVhX2JvdHRvbSI6IDcyNywKICAg\
ICAgICAgIndvcmtfYXJlYV9sZWZ0IjogMCwKICAgICAgICAgIndvcmtfYXJlYV9y\
aWdodCI6IDEyODAsCiAgICAgICAgICJ3b3JrX2FyZWFfdG9wIjogMjIKICAgICAg\
fQogICB9LAogICAiY291bnRyeWlkX2F0X2luc3RhbGwiOiAxNjcyNSwKICAgImRl\
ZmF1bHRfYXBwc19pbnN0YWxsX3N0YXRlIjogMywKICAgImRpc3RyaWJ1dGlvbiI6\
IHsKICAgICAgImltcG9ydF9ib29rbWFya3MiOiBmYWxzZSwKICAgICAgImltcG9y\
dF9oaXN0b3J5IjogZmFsc2UsCiAgICAgICJpbXBvcnRfaG9tZV9wYWdlIjogZmFs\
c2UsCiAgICAgICJpbXBvcnRfc2VhcmNoX2VuZ2luZSI6IGZhbHNlLAogICAgICAi\
c2hvd193ZWxjb21lX3BhZ2UiOiBmYWxzZSwKICAgICAgInNraXBfZmlyc3RfcnVu\
X3VpIjogdHJ1ZSwKICAgICAgInN1cHByZXNzX2ZpcnN0X3J1bl9idWJibGUiOiB0\
cnVlLAogICAgICAic3VwcHJlc3NfZmlyc3RfcnVuX2RlZmF1bHRfYnJvd3Nlcl9w\
cm9tcHQiOiB0cnVlCiAgIH0sCiAgICJmaXJzdF9ydW5fdGFicyI6IFsgIiIgXSwK\
ICAgImhvbWVwYWdlIjogIiIsCiAgICJob21lcGFnZV9pc19uZXd0YWJwYWdlIjog\
ZmFsc2UsCiAgICJob3R3b3JkIjogewogICAgICAicHJldmlvdXNfbGFuZ3VhZ2Ui\
OiAiZW4tVVMiCiAgIH0sCiAgICJpbnRsIjogewogICAgICAiYWNjZXB0X2xhbmd1\
YWdlcyI6ICJlbi1VUyxlbiIKICAgfSwKICAgImludmFsaWRhdG9yIjogewogICAg\
ICAiY2xpZW50X2lkIjogIk1Xa0pzSWI1MFZrRmZqSmIxbEg4NVE9PSIKICAgfSwK\
ICAgIm1lZGlhIjogewogICAgICAiZGV2aWNlX2lkX3NhbHQiOiAicVBJUG1mVjl1\
N0NKR3M2YVh4OUR4QT09IgogICB9LAogICAicGlubmVkX3RhYnMiOiBbICBdLAog\
ICAicGx1Z2lucyI6IHsKICAgICAgIm1pZ3JhdGVkX3RvX3BlcHBlcl9mbGFzaCI6\
IHRydWUsCiAgICAgICJwbHVnaW5zX2xpc3QiOiBbICBdLAogICAgICAicmVtb3Zl\
ZF9vbGRfY29tcG9uZW50X3BlcHBlcl9mbGFzaF9zZXR0aW5ncyI6IHRydWUKICAg\
fSwKICAgInByb2ZpbGUiOiB7CiAgICAgICJhdmF0YXJfaW5kZXgiOiAyNiwKICAg\
ICAgImNvbnRlbnRfc2V0dGluZ3MiOiB7CiAgICAgICAgICJjbGVhcl9vbl9leGl0\
X21pZ3JhdGVkIjogdHJ1ZSwKICAgICAgICAgInBhdHRlcm5fcGFpcnMiOiB7Cgog\
ICAgICAgICB9LAogICAgICAgICAicHJlZl92ZXJzaW9uIjogMQogICAgICB9LAog\
ICAgICAiZXhpdF90eXBlIjogIk5vcm1hbCIsCiAgICAgICJleGl0ZWRfY2xlYW5s\
eSI6IHRydWUsCiAgICAgICJtYW5hZ2VkX3VzZXJfaWQiOiAiIiwKICAgICAgIm5h\
bWUiOiAiTk9DIiwKICAgICAgInBhc3N3b3JkX21hbmFnZXJfZW5hYmxlZCI6IGZh\
bHNlLAogICAgICAicGVyX2hvc3Rfem9vbV9sZXZlbHMiOiB7CgogICAgICB9CiAg\
IH0sCiAgICJwcm94eSI6IHsKICAgICAgImJ5cGFzc19saXN0IjogIiIsCiAgICAg\
ICJtb2RlIjogInN5c3RlbSIsCiAgICAgICJzZXJ2ZXIiOiAiIgogICB9LAogICAi\
c2Vzc2lvbiI6IHsKICAgICAgInJlc3RvcmVfb25fc3RhcnR1cCI6IDQsCiAgICAg\
ICJyZXN0b3JlX29uX3N0YXJ0dXBfbWlncmF0ZWQiOiB0cnVlLAogICAgICAic3Rh\
cnR1cF91cmxzIjogWyAiIiBdLAogICAgICAic3RhcnR1cF91cmxzX21pZ3JhdGlv\
bl90aW1lIjogIjEzMDY0OTk1MzYyNDE4ODgzIgogICB9LAogICAic3luY19wcm9t\
byI6IHsKICAgICAgInVzZXJfc2tpcHBlZCI6IHRydWUKICAgfSwKICAgInRyYW5z\
bGF0ZV9ibG9ja2VkX2xhbmd1YWdlcyI6IFsgImVuIiBdLAogICAidHJhbnNsYXRl\
X3doaXRlbGlzdHMiOiB7CgogICB9Cn0K'


def do_urls4nagii():
    urls4nagii = []
    with open(file_example_aws_environment_registry) as data_file:
        data = json.load(data_file)
        for key, value in sorted(data.iteritems()):
              urls4nagii.append(nagios_proto + key + nagios_urluri)
    return urls4nagii

def do_create_chrome_profile(profile):
    os.mkdir(path + '/' + profile, 0755 );
    file = open(path + '/' + profile + '/Preferences', 'w')
    file.write((base64.b64decode(b64_preferences)))
    file.close()
    return

def do_chrome_profile():
    path = os.environ['HOME'] + '/Library/Application Support/Google/Chrome'
    prefs = []
    for root, dirs, files in os.walk(path):
        for d in dirs:
            if 'Profile ' in d:
               prefs.append(root + '/' + d + '/' + 'Preferences')
    try:
        for a in range(len(prefs)):
            with open(prefs[a]) as data_file:
                data = json.load(data_file)
                if 'NOC' in data["profile"]["name"]:
                    prefs[a] = prefs[a].rsplit('/', 1)[0].rsplit('/', 1)[1]
                    profile = prefs[a]
    except: ''
    if len(prefs) == 0:
        profile = 'Profile 1'
        do_create_chrome_profile(profile)
        return profile, firstrun
    if len(prefs) == 1 and 'profile' not in locals():
        prefs[a] = prefs[a].rsplit('/', 1)[0].rsplit('/', 1)[1]
        prefs[a] = int(prefs[a][8])
        profile = 'Profile ' + str(prefs[a] + 1)
        do_create_chrome_profile(profile)
        firstrun = 'Y'
        return profile, firstrun
    if len(prefs) > 1:
        for a in range(1, len(prefs)):
            prefs[a] = prefs[a].rsplit('/', 1)[0].rsplit('/', 1)[1]
            prefs[a] = int(prefs[a][8])
        profile = 'Profile ' + str((sum(xrange(prefs[a],prefs[-1]+1)) - sum(prefs)))
        do_create_chrome_profile(profile)
        firstrun = 'Y'
        return profile, firstrun
    return profile, None


def do_chrome(chrome_profile, firstrun, urls, urls4nagii):
    if firstrun:
        firstrun = ' -first-run'
    else: firstrun = ''
    command_line="/Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome --profile-directory='" + chrome_profile + "'" + firstrun + ' --enable-automatic-password-saving'
    #Add urllib/urllib2 stuff for auto login
    args = shlex.split(command_line)
    FNULL = open(os.devnull, 'w')
    subprocess.Popen(args, stdout=FNULL, stderr=subprocess.STDOUT)
    time.sleep(3)
    for a in range(len(urls)):
        webbrowser.get().open_new_tab(urls[a])
    for a in range(len(urls4nagii)):
        webbrowser.get().open(urls4nagii[a])

if __name__ == '__main__':
    urls4nagii = do_urls4nagii()
    chrome_profile, firstrun = do_chrome_profile()
    do_chrome(chrome_profile, firstrun, urls, urls4nagii)
