**Makefile**
```
$ make
Actions...
cr_job:	 Scan pwd (../pit-hybrid/jenkins-pipelines) and creates jobs in Jenkins
	excluding the Template directory

rm_job:	 Scans pwd (../pit-hybrid/jenkins-pipelines) and remove jobs from Jenkins
	 excluding the Template directory

bk_job:	 Scans pwd (../pit-hybrid/jenkins-pipelines) and backup jobs from Jenkins
	 excluding the Template directory

Additional target args...
FLAG=jobname ... Specify job name

```

**Execute job manually using default parameters**
```
$ curl -X POST -u ${JENKINS_USER}:${JENKINS_TOKEN} "http://10.19.115.235:8080/job/deploy-ocp4-on-aws/buildWithParameters?delay=0sec"
```

**Execute job manually parameter value overrides**
```
$ curl -X POST -u ${JENKINS_USER}:${JENKINS_TOKEN} "http://10.19.115.235:8080/job/deploy-ocp4-on-aws/buildWithParameters?delay=0sec&CLUSTER_NAME=derp.us-east-1&REGION=us-east-1"
```
