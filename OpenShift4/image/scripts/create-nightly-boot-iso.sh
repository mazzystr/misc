#!/usr/bin/bash

BRANCH=${BRANCH:-master}
DISTRO=$(cat .DISTRO 2> /dev/null || echo el8)
NEWISO=${1:-$(realpath .)/appliance-${VERSION}-$(date +%Y%m%d%H).${DISTRO}.iso}
BOOTISO=${BOOTISO:-boot.iso}
SQUASHFS=${SQUASHFS:-appliance.squashfs.img}
PRODUCTIMG=${PRODUCTIMG:-product.img}
DERVICEBOOTISOSCRIPT=${DERVICEBOOTISOSCRIPT:-`echo $PWD`/scripts/derive-boot-iso.sh}

cond_curl() {
  echo "Fetching $1 from $2 ";
  curl --fail -# -o "$1" $2;
}

get_bootiso() {
  case "${DISTRO}" in
    (centos)
      cond_curl "$BOOTISO" "http://packages.oit.ncsu.edu/centos/8/BaseOS/x86_64/os/images/boot.iso"
    ;;
    (fedora)
      cond_curl "$BOOTISO" "https://download.fedoraproject.org/pub/fedora/linux/releases/30/Server/x86_64/iso/Fedora-Server-netinst-x86_64-30-1.2.iso"
    ;;
    (rhel)
      cond_curl "$BOOTISO" "http://download-node-02.eng.bos.redhat.com/rhel-8/rel-eng/RHEL-8/latest-RHEL-8.0.0/compose/BaseOS/x86_64/iso/RHEL-8.0.0-20190404.2-BaseOS-x86_64-boot.iso"
    ;;
    (*)
      cond_curl "$BOOTISO" "http://download-node-02.eng.bos.redhat.com/rhel-8/rel-eng/RHEL-8/latest-RHEL-8.0.0/compose/BaseOS/x86_64/iso/RHEL-8.0.0-20190404.2-BaseOS-x86_64-boot.iso"
    ;;
  esac

}

get_productimg() {
  echo
  #cond_curl "$PRODUCTIMG" "http://jenkins.ovirt.org/job/ovirt-node-ng_${BRANCH}_build-artifacts-fc22-x86_64/lastStableBuild/artifact/exported-artifacts/product.img"

}

get_squashfs() {
  echo
  #cond_curl "$SQUASHFS" "http://jenkins.ovirt.org/job/ovirt-node-ng_${BRANCH}_build-artifacts-fc22-x86_64/lastStableBuild/artifact/exported-artifacts/ovirt-node-ng-image.squashfs.img"

}


set -e
echo "Building an Appliance iso from boot.iso and a nightly squashfs"
echo "This can take a while ..."

get_bootiso
PRODUCTIMG=$PRODUCTIMG bash $DERVICEBOOTISOSCRIPT "$BOOTISO" "$SQUASHFS" "$NEWISO"
echo "New installation ISO: $NEWISO"
