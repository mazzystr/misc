# Kickstart for KNI Appliance based on `make installed-squashfs` with some
# minor changes

lang en_US.UTF-8
keyboard us
timezone --utc Etc/UTC --ntpservers=clock.corp.redhat.com
network
auth --passalgo=sha512 --useshadow
selinux --enforcing
rootpw $1$+CIl2eze$5omiyRKnLdnja5QDvXPPV/ --iscrypted
user --groups=wheel,libvirt --homedir=/home/kni --name=kni --password=$1$+CIl2eze$5omiyRKnLdnja5QDvXPPV/ --iscrypted
bootloader --timeout=3 --append="rhgb quiet crashkernel=auto"
clearpart --all --initlabel
autopart
firewall --enabled --ssh --http
services --enabled=chronyd,firewalld,NetworkManager,sshd
skipx
text
install
firstboot --disable
poweroff

liveimg --url=file:///run/install/repo/appliance.squashfs.img

%pre
sysctl kernel.hostname=appliance

# Assumption: A virtio device with the serial livesrc is passed, pointing to the squashfs on the host.
mkdir -p /mnt/livesrc
mount /dev/disk/by-id/virtio-livesrc /mnt/livesrc
%end

%post
grub2-mkconfig -o /boot/grub2/grub.cfg

echo 'kernel.hostname=appliance' > /etc/sysctl.d/00_hostname.conf

# BEGIN HACKARY NOW!
# For some reason we 
sed -i 's/\/boot//g' /boot/loader/entries/*x86_64.conf

%end
