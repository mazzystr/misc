**Usage**
```
# GENERATE DYNAMIC FILES
$ ./autogen.sh # Defaults to --with-distro=rhel
$ ./autogen.sh --with-distro={centos|fedora|rhel}

# CREATE SQUASHFS
$ make squashfs

# CREATE ISO
$ make iso

# CLEAN UP
$ make clean
```

**Testing**

- Virtualbox
```
$ VM=blah

$ ISO=/path/to/appliance*.iso

$ scp user@target:/path/to/appliance*.iso .

# TODO ... Create command to create opinionated virtual machine

$ STOP 2> /dev/null; VBoxManage controlvm ${VM} poweroff

$ MOUNT 2> /dev/null; VBoxManage storageattach ${VM} --storagectl IDE --port 1 --device 0 --type dvddrive --medium appliance*.iso

$ START 2> /dev/null; VBoxManage startvm --type separate ${VM}

# Virtual Machine will power up, kickstart, and power down
 
$ EJECT 2> /dev/null; VBoxManage storageattach ${VM} --storagectl IDE --port 1 --device 0 --medium none

$ START 2> /dev/null; VBoxManage startvm --type separate ${VM}
```
