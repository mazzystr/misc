lang en_US
keyboard us
timezone America/New_York --isUtc --ntpservers=clock.corp.redhat.com
rootpw $1$+CIl2eze$5omiyRKnLdnja5QDvXPPV/ --iscrypted
user --groups=wheel --homedir=/home/kni --name=kni --password=$1$+CIl2eze$5omiyRKnLdnja5QDvXPPV/ --iscrypted
reboot
text
cdrom
bootloader --location=mbr --append="rhgb quiet crashkernel=auto"
zerombr
clearpart --all --initlabel
autopart
auth --passalgo=sha512 --useshadow
firewall --enabled --ssh --http
services --enabled=chronyd,firewalld,NetworkManager,sshd
skipx
firstboot --disable
%packages
@^minimal-environment
ansible
bind-utils
curl
genisoimage
git
jq
libvirt
libvirt-daemon-kvm
make
NetworkManager
nmap
podman
python36
python3-lxml
python3-netaddr
python3-requests
python3-setuptools
tar
tmux
vim-minimal
wget
%end

%include /tmp/network.ks

%pre
NIC1="$(ip -o link show | sort -k2 | awk -F': ' 'NR==1  { print $2  }')"
NIC2="$(ip -o link show | sort -k2 | awk -F': ' 'NR==2  { print $2  }')"
echo "network --bootproto=static --ip=172.22.0.1 --netmask=255.255.255.0 --device=provisioning --bridgeslaves=${NIC1} --onboot=yes --nodefroute --noipv6 --activate" > /tmp/network.ks
echo "network --bootproto=dhcp --device=baremetal --bridgeslaves=${NIC2} --onboot=yes --noipv6 --activate" >> /tmp/network.ks
%end

%post --interpreter=/bin/bash
echo 'ZONE=libvirt' >> /etc/sysconfig/network-scripts/ifcfg-provisioning
echo "kni ALL=(root) NOPASSWD:ALL" | tee -a /etc/sudoers.d/kni
chmod 0440 /etc/sudoers.d/kni
alternatives --set python /usr/bin/python3
pushd /root
oc_version=4.2
oc_tools_dir=$HOME/oc-${oc_version}
oc_tools_local_file=openshift-client-${oc_version}.tar.gz
mkdir -p ${oc_tools_dir}
cd ${oc_tools_dir}
wget https://mirror.openshift.com/pub/openshift-v4/clients/oc/${oc_version}/linux/oc.tar.gz -O ${oc_tools_local_file}
tar xvzf ${oc_tools_local_file}
cp oc /usr/local/bin/

cat > /etc/systemd/system/sshkeysetup.service << EOF
[Unit]
Description=Create an ssh key for kni user
Requires=sshd.service
After=sshd.service
 
[Service]
Type=oneshot
User=kni
Group=kni
ExecStart=/bin/ssh-keygen -t rsa -f /home/kni/.ssh/id_rsa -N ''
 
[Install]
WantedBy=multi-user.target
EOF

/usr/bin/systemctl enable sshkeysetup

cat > /usr/bin/virshsetup.sh  << EOT
#!/usr/bin/env bash
set -xe

export USER=kni
# Restart libvirtd service to get the new group membership loaded
if ! id \$USER | grep -q libvirt; then
  sudo usermod -a -G "libvirt" \$USER
  sudo systemctl restart libvirtd
fi

# As per https://github.com/openshift/installer/blob/master/docs/dev/libvirt-howto.md#configure-default-libvirt-storage-pool
# Usually virt-manager/virt-install creates this: https://www.redhat.com/archives/libvir-list/2008-August/msg00179.html
if ! virsh pool-uuid default > /dev/null 2>&1 ; then
    virsh pool-define /dev/stdin <<EOR
<pool type='dir'>
  <name>default</name>
  <target>
    <path>/var/lib/libvirt/images</path>
  </target>
</pool>
EOR
    virsh pool-start default
    virsh pool-autostart default
fi

# Create the provisioning bridge
if ! virsh net-uuid provisioning > /dev/null 2>&1 ; then
    virsh net-define /dev/stdin <<EOS
<network>
  <name>provisioning</name>
  <bridge name='provisioning'/>
  <forward mode='bridge'/>
</network>
EOS
    virsh net-start provisioning
    virsh net-autostart provisioning
fi

# Create the baremetal bridge
if ! virsh net-uuid baremetal > /dev/null 2>&1 ; then
    virsh net-define /dev/stdin <<EOF
<network>
  <name>baremetal</name>
  <bridge name='baremetal'/>
  <forward mode='bridge'/>
</network>
EOF
    virsh net-start baremetal
    virsh net-autostart baremetal
fi
EOT

chmod +x /usr/bin/virshsetup.sh

cat > /etc/systemd/system/virshsetup.service << EOF
[Unit]
Description=Set default storage pool and net bridges
Requires=libvirtd.service
After=libvirtd.service

[Service]
Type=oneshot
ExecStart=/usr/bin/virshsetup.sh

[Install]
WantedBy=multi-user.target
EOF

/usr/bin/systemctl enable virshsetup

cat > /home/kni/Makefile << EOM
.PHONY: default clone deploy bell

default: clone deploy

clone:
	git clone https://github.com/openshift-kni/install-scripts.git; pushd install-scripts; git pull -r; popd

deploy: 
	set -e; cp pull-secret* install-scripts/preflight/; pushd install-scripts; make; popd

bell:
	@echo "Done!" \$\$'\a'
EOM

chown kni:kni /home/kni/Makefile

cat > /root/isoversion << EOV
version 11
EOV

firewall-offline-cmd --zone=libvirt --add-port=80/tcp
firewall-offline-cmd --zone=libvirt --add-port=80/tcp --permanent

%end
