**Makefile**
```
$ make
OPENSHIFT_CLUSTER_NAME is undefined
Since you please the Fifth an OPENSHIFT_CLUSTER_NAME env var will be assigned to you

Please execute the following...
export OPENSHIFT_CLUSTER_NAME=15d822026dfa

Actions...
update_citoken:	 CI tokens expire every 30 days.  It must be periodically updated.
latest_cli:	 Pull latest OpenShift client release
latest_bin:	 Pull latest bin/openshift-install release
                 Additional target args...
                 FLAG=ci: Retrieve last ci release
                 FLAG=nightly: Retrieve last ci nightly release
                 FLAG=stable: Retrieve last ci stable release
                 FLAG=manual{ci,nightly,stable}: Echo commands to retrieve different releases
create_ic:	 Create a install_config file within /tmp/openshift/${OPENSHIFT_CLUSTER_NAME}
create_clu:	 Create an OpenShift 4.x cluster.  Misc installation files will be located within /tmp/openshift/${OPENSHIFT_CLUSTER_NAME}
destroy_clu:	 Destroy an OpenShift 4.x cluster using the files within /tmp/openshift/${OPENSHIFT_CLUSTER_NAME}
clean:	         Clean up working directory /tmp/openshift/${OPENSHIFT_CLUSTER_NAME}
```
