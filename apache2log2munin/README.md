```
usage: fh_apache2log2munin.py [-h] [-al apache_log] [-alt apache_log_type]
                                  [-s delta] [-mo munin_opts] [-u uri_filter]
                                  [config] [debug]

positional arguments:
  config                Output Munin graph configuration
  debug                 Set debug level (default: 0) (options: [0-3])

optional arguments:
  -h, --help            show this help message and exit
  -al apache_log, --apache_log apache_log
                        Specify Apache log (options: default, other_vhosts,
                        [studio])
  -alt apache_log_type, --apache_log_type apache_log_type
                        Specify Apache access log (options: [access], error)
  -s delta, --delta delta
                        Relative time delta in hours from now (default: 1)
  -mo munin_opts, --munin_opts munin_opts
                        Munin graph information in json format (example:
                        '{"title": "ups_push", "info": "This graph represents
                        the rate of ups_push api accesses in the last hour",
                        "category":"other", "label": "ups_push", "vlabel":
                        "count per hour"}')
  -u uri_filter, --uri_filter uri_filter
                        uri's to search for in python dictionary format
                        (default: ['/api/v2/ag-push','/box/api/unifiedpush'])
```
