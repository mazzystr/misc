#!/usr/bin/env python

from datetime import datetime, timedelta
from subprocess import call
import argparse
import multiprocessing
import os

parser = argparse.ArgumentParser()

parser.add_argument(
    'config', metavar='config', nargs='?',
    help='Output Munin graph configuration')
parser.add_argument(
    'debug', metavar='debug', nargs='?', type=int, default=0,
    help='Set debug level \
    (default: 0) (options: [0-3])')
parser.add_argument(
    '-al', '--apache_log', metavar='apache_log', default='studio',
    help='Specify Apache log (options: default, other_vhosts, [studio])')
parser.add_argument(
    '-alt', '--apache_log_type', metavar='apache_log_type', default='access',
    help='Specify Apache access log (options: [access], error)')
parser.add_argument(
    '-s', '--delta', metavar='delta', type=int, default=1,
    help='Relative time delta in hours from now (default: 1)')
parser.add_argument(
    '-mo', '--munin_opts', metavar='munin_opts',
    default={"title": "ups_push",
             "info": "This graph represents the rate of ups_push api " +
             "accesses in the last hour",
             "category": "other",
             "label": "ups_push",
             "vlabel": "count per hour",
             },
    help='Munin graph information in json format \
    (example: \
    \'{"title": "ups_push", \
    "info": "This graph represents the rate of ups_push api accesses in the \
    last hour", \
    "category":"other", \
    "label": "ups_push", \
    "vlabel": "count per hour"}\')')
parser.add_argument(
    '-u', '--uri_filter', metavar='uri_filter',
    default=['/api/v2/ag-push', '/box/api/unifiedpush'],
    help="uri's to search for in python dictionary format \
    (default: ['/api/v2/ag-push','/box/api/unifiedpush'])")
args = parser.parse_args()


def find_files(apache_log, apache_log_type):
    files = ['/var/log/apache2/' + apache_log + '-' + apache_log_type +
             '.log']
    if f_datetime('H') == str(0):
        yesterday = f_datetime('yesterday').split()
        files.append('/var/log/apache2/' + apache_log + '-' +
                     apache_log_type + 'log' + '-' + yesterday[0] + '-' +
                     yesterday[1] + '-' + yesterday[2] + '.gz')
    return(files)


def get_chunks(file_size):
    chunk_start = 0
    chunk_size = 0x4000000  # 131072 bytes
    while ((chunk_start + chunk_size) < file_size):
        yield(chunk_start, chunk_size)
        chunk_start += chunk_size

    final_chunk_size = file_size - chunk_start
    yield(chunk_start, final_chunk_size)


def f_datetime(arg):
    if str.isdigit(str(arg)):
        return((datetime.now() - timedelta(hours=arg)).
               strftime('%b %d %Y %H:%M:%S'))
    elif arg == 'H':
        return(datetime.now().strftime('%H'))
    elif arg == 'mon2num':
        return(datetime.now().strftime('%H'))
    elif arg == 'now':
        return(datetime.now().strftime('%b %d %Y %H:%M:%S'))
    elif arg == 'yesterday':
        return((datetime.now() - timedelta(days=1)).strftime('%Y %m %d'))


def output_config(metrics, munin_opts):
    print('graph_title ' + munin_opts['title'])
    print('graph_info ' + munin_opts['info'])
    print('graph_category ' + munin_opts['category'])
    print('graph_vlabel ' + munin_opts['vlabel'])
    print('default.label default')
    for k, v in metrics.iteritems():
        print(k.replace('.', '_') + '.label ' + k.replace('.', '_'))


def output_munin(metrics):
    for k, v in metrics.iteritems():
        print(k.replace('.', '_') + '.value ' + str(v))


def parse_logs(args, q, lines):
    metrics = {}
    lines = lines.split("\n")
    # print(len(lines))
    try:
        if 'config' in args.config:
            args.delta = 24
    except:
        pass
    d_datetime = f_datetime(args.delta).split()
    # print(d_datetime)
    a = datetime.strptime(d_datetime[0] + ' ' + d_datetime[1] + ' ' +
                          d_datetime[2] + ' ' + d_datetime[3],
                          '%b %d %Y %H:%M:%S')
    # print(a)
    for line in lines:
        for uri in args.uri_filter:
            if uri in line:
                for ch in ['[', ']', '"', ',', "+"]:
                    line = line.replace(ch, '')
                # print(line)
                line = line.split()
                # print(len(line))
                if (len(line) == 17):
                    # print(line[5])
                    log_datetime = (line[5].split(':'))
                    # print(log_datetime)
                    log_date = log_datetime[0].split('/')
                    # print(log_date)
                    b = datetime.strptime(log_date[1] + ' ' + log_date[0] +
                                          ' ' + log_date[2] + ' ' +
                                          log_datetime[1] + ':' +
                                          log_datetime[2] + ':' +
                                          log_datetime[3],
                                          '%b %d %Y %H:%M:%S')
                    # print(a, ' : ', b)
                    if a < b:
                        # print(line[12])
                        if line[12] not in metrics:
                            # print(metrics)
                            # print('first :', line[12])
                            metrics.update({line[12]: 0})
                        # print(metrics[line[12]:0])
                        # print(line)
                        metrics[line[12]] += 1
                        # print(metrics[line[12]])

    # print(metrics)
    for k, v in metrics.iteritems():
        q.put((k, v))


def main():

    jobs = []
    metrics = {}

    p = multiprocessing.Pool(1)
    q = multiprocessing.Queue()

    files = find_files(args.apache_log, args.apache_log_type)

    for file in files:
        if 'gz' in file:
            call(["gunzip", file])
            t_file = file.replace('.gz', '')
        else:
            t_file = file
        try:
            with open(t_file) as file_:
                file_size = os.path.getsize(t_file)
                for chunk_start, chunk_size in get_chunks(file_size):
                    file_chunk = file_.read(chunk_size)
                    p = multiprocessing.Process(target=parse_logs,
                                                args=(args, q, file_chunk))
                    jobs.append(p)
        except:
            print("There was a problem opening file : %s", file)

    for j in jobs:
        j.start()

    for j in jobs:
        j.join()

    while not q.empty():
        m = q.get()
        # print(m[0],m[1])
        if m[0] in metrics:
            metrics[m[0]] += m[1]
        else:
            metrics.update({m[0]: m[1]})

    if args.config:
        output_config(metrics, args.munin_opts)

    if not args.config:
        output_munin(metrics)

    for file in files:
        if 'gz' in file:
            t_file = file.replace('.gz', '')
            call(["gzip", t_file])

if __name__ == '__main__':
    main()
