#!/usr/bin/perl

#Copyright (C)  Chris Callegari mazzystr@gmail.com
#https://github.com/mazzystr/scripts
#
#This program is free software; you can redistribute it and/or
#modify it under the terms of the GNU General Public License
#as published by the Free Software Foundation; either version 2
#of the License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#Inialize modules
use strict;
use warnings;
use Data::Dumper;
use Getopt::Long;

#Initialize variables
my (%IFACE, %SESSION, @IFACE_DATA, @SESSION_DATA, $IFACE_CMD, @val, %opts, @int,
    $avail_options, $val, $val_tmp, $offl, $mtu, $vlan, $sessions, $message,
    $STATE_OK, $STATE_WARN, $STATE_CRIT, $FILE, $STATE_UNKNOWN, $key, $int,
    $state, $i, $j, $k, @k);

#Set globals
use vars qw/ %opt /;
$STATE_OK = $state = 0;
$STATE_WARN=1;
$STATE_CRIT=2;
$STATE_UNKNOWN=3;

#Catch all usage output
sub usage {
  print STDERR << "EOF";
Usage: $0 [--offload (y|yes|n|no)] [--mtu (1500|9000)]
  [--vlan (1-4049) [--sessions [0-9]+] [ -? | -h | --help ]
Available options:
  --offload   hardware iSCSI offload, yes|no
                query \"iscsiadm -m iface -P1\" and parse
  --mtu       mtu size for iSCSI interfaces, 1500|9000
                if offload=n, query \"ip addr\" and parse mtu
                if offload=y, query \"iscsiadm -m iface -I \$I | grep iface.mtu\"
  --vlan      vlan id, 1-4094 (IEEE 802.1Q standard)
                if offload=n,
                  query \"iscsiadm -m host\" -> obtain IP of initiator
                 \"ip addr | grep IP\" -> obtain interface name
                 \"cat /sys/class/net/eth#/mtu\" -> obtain mtu value
                if offload=y, \"iscsiadm -m iface -I \$I -o show | grep vlan_id\"
  --sessions  total number active iSCSI sessions, (0-9)+
                query \"iscsiadm -m session -r SID -P3\" and parse
NOTES:
  This plugin only issues state OK|WARN
  ONLY works for the following interfaces...
    Standard Linux network interfaces
    Broadcom Corporation NetXtreme II 10 Gigabit Ethernet
    Chelsio Communications Inc 10GbE Port Adapters
    Send me lspci, \"iscsiadm -m iface -P1\", \"iscsiadm -m host\"
  VLAN is experimental.  The code looks like it should work but we don't vlan
                         tag storage interfaces so I have no a test bed.
EOF
  exit 1;
}

sub init() {
  if ( scalar(@ARGV) == 0 ) { usage(); }
  $avail_options = 'offload:mtu:vlan:sessions:h:help';
  GetOptions(\%opts,
    "help|h|?",
    "offload=s"   => \$offl,
    "mtu=s"       => \$mtu,
    "vlan=s"      => \$vlan,
    "sessions=s"  => \$sessions,
  ) or usage();
if ($opts{'help'}) { usage(); }
}

#Evaluate arguements
init();

#Query iscsiadm sessions
opendir(DIR, "/sys/class/iscsi_session");
while( @val=readdir(DIR) ) {
  foreach (@val) {
    if ( /session/ ) {
      s/^session//;
      $key = "SID:" .$_;
      $SESSION{$key} = "1";
      open(A, "iscsiadm -m session -r $_ -P3 |");
      @SESSION_DATA = <A>;
      $val_tmp = "1";
      foreach $val ( @SESSION_DATA ) {
        chomp $val;
        $val =~ s/^(\t)+//g;
        $val =~ s/: /:/g; $val =~ s/ /_/g;
          if ( $val =~ /^Target:/ )                { $val_tmp  = $val; }
          if ( $val =~ /^Iface_Name/ )             { $val_tmp .= " " . $val; }
          if ( $val =~ /^Iface_Transport/ )        { $val_tmp .= " " . $val; }
          if ( $val =~ /^iSCSI_Session_State/ )    { $val_tmp .= " " . $val; }
          if ( $val =~ /^iSCSI_Connection_State/ ) { $val_tmp .= " " . $val; }
          if ( $val =~ /^Attached_scsi_disk/ )     {
            $val =~ s/^Attached_scsi_disk_//g;
            $val =~ s/          State:running$//g;
            $val_tmp .= " iSCSI_Disk:" .$val; }
      }
      close A;
      if ( $val_tmp =~ /iSCSI_Disk/ ) { $SESSION{$key} = $val_tmp; }
      else                            { delete $SESSION{key}; }
    }
  }
}
close DIR;

#print Dumper (\%SESSION);

# STATE - iSCSI offload conditional
if ($offl) {
  foreach $key ( keys %SESSION ) {
    $key =~ s/^SID://g;
    open(A, "iscsiadm -m session -r $key |");
    while ($val=<A>) {
      chomp $val;
      if ( $val =~ /iface.transport_name = tcp/ ) {
        $j = "no";
        last;
      }
      elsif ( $val =~ /iface.transport_name = (bnx2i|cxgb3i|be2iscsi)/ ) {
        $j = "yes";
      }
    }
  }
  if ( $j !~ /$offl/ ) { $state=1; }
  $message = "iscsi_hardware_offload:$j; ";
  $offl = $j;
}

# STATE - network mtu conditional
if (($mtu) && ( $offl =~ /n/ )) {
  $i=""; $j="";
  $IFACE_CMD=`iscsiadm -m host`;
  @IFACE_DATA=split("\n", $IFACE_CMD);
  foreach $val ( @IFACE_DATA ) {
    chomp $val;
    if ( $val =~ /^tcp: / ) {
      $val =~ s/^.*\] //g; $val =~ s/,\[.*$//g;
      $int = `ip addr`;
      @int = split("\n", $int);
      foreach $i ( @int) {
        $i =~ s/^\s+//g;
        if ( $i =~ /inet $val *.* eth[0-9]+/ ) {
          @val = split(" ", $i);
          open(A, "cat /sys/class/net/$val[6]/mtu |");
          foreach ($k=<A>) {
            chomp $k;
            if ( $j !~ /$val[6]/ ) { $j = $j . "$val[6]:" . $k . ","; }
          }
          close A;
        }
      }
    }
  }
}
elsif (($mtu) && ( $offl =~ /y|yes/ )) {
  $i=""; $j="";
  foreach $key ( %IFACE ) {
    if (( $key =~ /bnx2i\./ ) && ( $IFACE{$key} !~ /^1$/ )) {
      open (A, "iscsiadm -m iface -I $key -o show |");
      while (<A>) {
        if ( /iface.mtu/ ) {
          chomp;
          @val = split(" = ", $_);
          $i=$val[1];
        }
      }
      foreach ($mtu) {
        if ( $j !~ /$key/ ) { $j = $j . "$key:" .$i .","; }
      }
      close A;
    }
  }
}
if ($mtu) {
  $j =~ s/,$//;
  @val = split(",", $j);
  foreach (@val) {
    if (( $state == 0 ) && ( /$mtu/ )) { $state = 0; }
    else                               { $state = 1; }
  }
  $message = $message . "mtu:$j; ";
}

# STATE - network vlanid conditional
if (($vlan) && ( $offl =~ /n|no/ )) {
  ($i, $j, $k) = "0";
  $IFACE_CMD=`iscsiadm -m host`;
  @IFACE_DATA=split("\n", $IFACE_CMD);
  foreach $val ( @IFACE_DATA ) {
    chomp $val;
    if ( $val =~ /^tcp: / ) {
      $val =~ s/^.*\] //g; $val =~ s/,\[.*$//g;
      $int = `ip addr`;
      @int = split("\n", $int);
      foreach $i ( @int) {
        $i =~ s/^\s+//g;
        if ( $i =~ /inet $val *.* eth[0-9]+\.[0-4094]/ ) {
          @val = split(" ", $i);
          open(A, "cat /proc/net/vlan/$val[6] |");
          foreach (<A>) {
            if ( /VID: [0-4094]/ ) {
              @k = split(": ", $_);
              if ( $vlan == $_ ) {
                if ( $j !~ /$val[6]/ ) { $j = $j . "$val[6]:" . @k . ","; } }
              else {
                if ( $j !~ /$val[6]/ ) { $j = $j . "$val[6]:" . @k . ","; } }
            }
          }
          close A;
        }
        else {
          if ( $i =~ /inet $val *.* eth[0-9]+/ ) {
            @val = split(" ", $i);
            if ( $j !~ /$val[6]/ ) { $j = $j . "$val[6]:not_tagged,"; } }
        }
      }
    }
  }
}
elsif (($vlan) && ( $offl =~ /y|yes/ )) {
  $j = "0";
  foreach $key ( %IFACE ) {
    if (( $key =~ /bnx2i\./ ) && ( "$IFACE{$key}" eq "1" )) {
      open (A, "iscsiadm -m iface -I $key -o show |");
      while (<A>) {
        if ( /iface\.vlan_id/ ) {
          chomp;
          @val = split(" = ", $_);
          if ( $val[1] == "0" ) {
            if ( $j !~ /$key/ ) { $j = $j . "$key:not_tagged,"; } }
          else {
            if ( $j !~ /$key/ ) { $j = $j . "$key:" .$val[1] .","; } }
        }
      }
      close A;
    }
  }
}
if ($vlan) {
  if ( "$i" != "$vlan" ) { $state = 1; }
$message = $message . "vlanid:$j ";
}

# STATE - iSCSI sessions conditional
if ($sessions) {
  $val = "";
  ($i, $j, $k) = 0;
  foreach $key ( keys %SESSION ) {
    $i++;
    if ( "$SESSION{$key}" !~ m/(^1$|LOGGED_IN)/ ) {
      $state = 2; $i--; $j++;
      $val = "," .$j . "-CRIT";
    }
  }
  $message = $message . "session_state:$i-OK$val;";
}

#OK   - iscsi_hardware_offload:yes, mtu:9000, vlan:eth4:9000,eth6:9000, session_state:12-OK
#                       WARN----^----^-------------------^---------^
#CRIT - iscsi_hardware_offload:yes, mtu:9000, vlan:eth4:9000,eth6:9000, session_state:3-OK,SID:1-iqn.2002-03.com.compellent:5000d31000364773-State:TRANSPORT_WAIT-/dev/sdc-CRIT
#                       CRIT-----------------------------------------------------------------------------------------------------------------------^

#Add trailing | so perfdata and long output can be attached
$message = $message . " |";

#FINAL OUTPUT - STATE / MESSAGE / LONG OUTPUT
if($state == $STATE_OK){
    $message = "OK - ".$message ."|\n";
} elsif($state == $STATE_WARN){
    $message = "WARNING - ".$message ."|\n";
} elsif($state == $STATE_CRIT){
    $message = "CRITICAL - ".$message ."|\n";
} else {
    $message = "UNKNOWN - ".$message ."|\n";
}

print $message;

exit $state;
